#!/bin/ruby

#
# Class to store the data for a frame
#
class Frame
  attr_reader :ball1
  attr_reader :ball2

  def initialize(ball1 = 0, ball2 = 0)
    @ball1 = ball1
    @ball2 = ball2
  end

  def basic_score
    @ball1 + @ball2
  end

  def spare?
    !strike? && (basic_score == 10)
  end

  def strike?
    @ball1 == 10
  end
end

#
# Simple score calculator class
#
class ScoreCalculator
  #
  # Loads the frames from a string containing the number of pins that fell and return an array
  # of 12 Frame objects with the data
  #
  def load_frames(pins)
    frames = []
    ball = 0

    # We load 12 frames as frame 10 may be a strike with 2 strikes following
    12.times do |f|
      # These will work as pins[x] returns null if it doesn't exist and null.to_i is 0
      this_ball = pins[ball].to_i
      next_ball = pins[ball + 1].to_i

      if this_ball == 10
        frames << Frame.new(10, 0)
        ball = ball + 1
      else
        frames << Frame.new(this_ball, next_ball)
        ball = ball + 2
      end
    end
    frames
  end

  #
  # Calculates the bonus for a spare from the next frame
  #
  def spare_bonus(next_frame)
    next_frame.ball1
  end

  #
  # Calulcates the bonus for a strike from the next two frames
  #
  def strike_bonus(next_frame1, next_frame2)
    if next_frame1.strike?
      next_frame1.ball1 + next_frame2.ball1
    else
      next_frame1.ball1 + next_frame1.ball2
    end
  end

  #
  # Calculates the score for a game from a string containing the number pins knocked down
  #
  def calculate_score(pins)
    frame = load_frames(pins)
    score = 0
    10.times do |f|
      score = score + frame[f].basic_score
      score = score + strike_bonus(frame[f + 1], frame[f + 2]) if frame[f].strike?
      score = score + spare_bonus(frame[f + 1]) if frame[f].spare?
    end
    score
  end
end

# Get our pins from the command line
pins = ARGV[0].split(" ")

# Calculate the score using the score calculator class
service = ScoreCalculator.new
puts service.calculate_score(pins)
